<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="person" class="com.example.servletjspdemo.domain.Person" scope="session" />

<jsp:setProperty name="person" property="*" /> 

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />

<%
if(storage.getSize()<6){
storage.add(person);
out.println("<p>Following person has been added to storage: </p>"+
		"<p>Imie:" + person.getName() +"</p>"+
		"<p>Nazwisko:" + person.getSurname() +"</p>"+
		"<p>Email:" + person.getEmail() +"</p>");
		
		String selectedInfo = "";
		if(request.getParameterValues("info") != null)
		for (String info : request.getParameterValues("info")) {
			selectedInfo += info + " ";
		}

		out.print(selectedInfo);
	
		out.print("<p>Pracodawca:"+ person.getEmployer() + "</p>");
}
else{
	out.print("<p>Brak wolnych miejsc. Twoje zgłoszenie nie zostało przyjęte</p>");
}
%>

<p>
  <a href="showAllPersons.jsp">Show all persons</a>
</p>
</body>
</html>