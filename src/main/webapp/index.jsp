<%@page import="java.awt.Stroke"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista</title>
    </head>
    <body>
    	
        <h2>Rejestracja na konferencję "Java 4 US!"</h2>
        <%
        if(session.getAttribute("zalogowany") == "juz_dodane")
        	out.print("<p>Dodałeś już swoje zgłoszenie</p>") ;       	
		else if( storage.getSize()<6)
        	out.println("<p><a href='getPersonData.jsp'>Fill a Person form</a></p>");     	
        else
        	out.println("Brak wolych miejsc");
        %>
    </body>
</html>
