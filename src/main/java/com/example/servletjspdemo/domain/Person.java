package com.example.servletjspdemo.domain;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String name;
	private String surname;
	private String email;
	private String employer;
	private String what_you_do;
	
	public Person() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getWhat_you_do() {
		return what_you_do;
	}

	public void setWhat_you_do(String what_you_do) {
		this.what_you_do = what_you_do;
	}
	
}