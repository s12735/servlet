package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"Imie: <input type='text' name='name' /> <br />" +
				"Nazwisko: <input type='text' name='surname' /> <br />" +
				"Email: <input type='email' name='email' /> <br />" +
				"Email: <input type='email' name='email' /> <br />" +
				"Pracodawca: <input type='text' name='employer' /> <br />" +
				"<input type='checkbox' name='info' value='Ogloszenie z pracy'>Ogloszenie w pracy<br />" +
				"<input type='checkbox' name='info' value='Ogloszenie na uczelni'>Ogloszenie na uczelni<br />" +
				"<input type='checkbox' name='info' value='Facebook'>Facebook<br />" +
				"<input type='checkbox' name='info' value='Znajomi'>Znajomi<br />" +
				"Czym sie zajmujesz?<br /> <textarea rows='4' cols='50' name='what_you_do'> </textarea><br />" +
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
