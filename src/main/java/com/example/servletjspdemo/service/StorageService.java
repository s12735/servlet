package com.example.servletjspdemo.service;


import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.Person;

public class StorageService {
	
	private List<Person> db = new ArrayList<Person>();
	
	public void add(Person person){
		Person newPerson = new Person();
		newPerson.setName(person.getName());
		newPerson.setSurname(person.getSurname());
		newPerson.setEmail(person.getEmail());
		newPerson.setEmployer(person.getEmployer());
		db.add(newPerson);
	}
	
	public List<Person> getAllPersons(){
		return db;
	}
	
	public int getSize(){
		return db.size();
	}

}
